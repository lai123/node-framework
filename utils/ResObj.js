
let ResObj = {
    fail: function (code, msg) {
        let returnData = { code: code, data: null, msg };
        return JSON.stringify(returnData);
    },
    success: function (data, msg = '请求成功') {
        let returnData = { code: 200, data: data, msg };
        return JSON.stringify(returnData);
    }
}

module.exports=ResObj