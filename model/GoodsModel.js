const { DataTypes, Model } = require('sequelize');

let sequelize = require("./BaseModel");

class Goods extends Model { }

Goods.init({
    goodsId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    goodsName: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    goodsImg: {
        type: DataTypes.STRING,
        allowNull: false,
    }

}, {
    // 这是其他模型参数
    sequelize, // 我们需要传递连接实例
    modelName: 'Goods', // 我们需要选择模型名称
    tableName: 'goods',
});

// 定义的模型是类本身

Goods.sync();

module.exports = Goods;