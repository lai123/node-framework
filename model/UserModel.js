const { DataTypes, Model } = require('sequelize');

let sequelize = require("./BaseModel");

class User extends Model { }

User.init({
    userId: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false,
    },
    name: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    direction: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    motto: {
        type: DataTypes.STRING,
        allowNull: false,
    }


}, {
    // 这是其他模型参数
    sequelize, // 我们需要传递连接实例
    modelName: 'User', // 我们需要选择模型名称
    tableName: 'user',
});

// 定义的模型是类本身

User.sync();

module.exports = User;